var gulp = require('gulp'),
    del = require('del'),
    less = require('gulp-less'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
    jshint = require('gulp-jshint'),
    gutil = require('gulp-util'),
    runSequence = require('run-sequence'),
    server = require('gulp-express'),
    browserSync = require('browser-sync'),

    DESTINATION_ROOT_DIRECTORY = './dist',
    DESTINATION_LIB_DIRECTORY = DESTINATION_ROOT_DIRECTORY + '/lib',
    DESTINATION_FONTS_DIRECTORY = DESTINATION_ROOT_DIRECTORY + '/fonts',
    DESTINATION_CSS_DIRECTORY = DESTINATION_ROOT_DIRECTORY + '/css',

    MOCK_SERVER_PATH = './test/mock/server.js',

    MAIN_SCRIPT_FILE_NAME = 'main.js',
    MAIN_CSS_FILE_NAME = 'main.css';

gulp.task('copy-index', function () {
   return gulp.src('src/index.html')
       .pipe(gulp.dest(DESTINATION_ROOT_DIRECTORY))
       .pipe(reload());
});

gulp.task('copy-libraries', function () {
   return gulp.src([
       'bower_components/angular/angular.js'
   ])
       .pipe(gulp.dest(DESTINATION_LIB_DIRECTORY));
});

gulp.task('copy-fonts', function () {
   return gulp.src('bower_components/bootstrap/fonts/**/*')
       .pipe(gulp.dest(DESTINATION_FONTS_DIRECTORY));
});

gulp.task('scripts', function () {
    return gulp.src('src/**/*.js')
        .pipe(plumber())
        .pipe(concat(MAIN_SCRIPT_FILE_NAME))
        .pipe(gulp.dest(DESTINATION_ROOT_DIRECTORY))
        .pipe(reload());
});

gulp.task('styles', function () {
    return gulp.src('src/assets/styles/main.less')
        .pipe(plumber())
        .pipe(less())
        .pipe(rename(MAIN_CSS_FILE_NAME))
        .pipe(gulp.dest(DESTINATION_CSS_DIRECTORY))
        .pipe(reload());
});

gulp.task('build', ['copy-index', 'copy-libraries', 'copy-fonts', 'styles', 'scripts']);

gulp.task('clean', function (done) {
    del('dist', done);
});

gulp.task('server', function () {
    server.run([MOCK_SERVER_PATH]);
    gulp.watch([MOCK_SERVER_PATH], [server.run]);
    browserSync({
        proxy: 'localhost:7775',
        browser: 'google chrome'
    });
});

gulp.task('watch', function () {
    gulp.watch('src/**/*.js', ['scripts']);
    gulp.watch('src/index.html', ['copy-index']);
    gulp.watch('src/assets/styles/**/*.less', ['styles']);
});

gulp.task('default', function (done) {
    runSequence(
        'clean',
        'build',
        'watch',
        'server',
        done
    );
});

function reload () {
    if (server) {
        return browserSync.reload({ stream: true });
    } else {
        return gutil.noop();
    }
}
