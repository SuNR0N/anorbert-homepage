angular.module(
    'naApp.directives',
    [
        'naApp.directives.header',
        'naApp.directives.footer'
    ]
);
