var express = require('express'),
    path = require('path'),
    app = express(),

    WEBAPP_DIRECTORY = createSystemFileName('../../dist'),
    PORT = 7775;

// given a file name path relative to this directory, create an absolute path
function createSystemFileName(fileName) {
    return path.resolve(path.join(__dirname, fileName));
}

app.use(express.static(WEBAPP_DIRECTORY));
app.listen(PORT);
